(function($) {

	skel.breakpoints({
		xlarge: '(max-width: 1680px)',
		large: '(max-width: 1280px)',
		medium: '(max-width: 980px)',
		small: '(max-width: 736px)',
		xsmall: '(max-width: 480px)',
		xxsmall: '(max-width: 360px)'
	});

	$(function() {

		var	$window = $(window),
			$body = $('body');

		// Disable animations/transitions until the page has loaded.
			$body.addClass('is-loading');

			$window.on('load', function() {
				window.setTimeout(function() {
					$body.removeClass('is-loading');
                    loadTwitch();
				}, 100);
			});

		// Fix: Placeholder polyfill.
			$('form').placeholder();

		// Prioritize "important" elements on medium.
			skel.on('+medium -medium', function() {
				$.prioritize(
					'.important\\28 medium\\29',
					skel.breakpoint('medium').active
				);
			});

		// Items.
			$('.item').each(function() {

				var $this = $(this),
					$header = $this.find('header'),
					$a = $header.find('a'),
					$img = $header.find('img');

				// Set background.
					$a.css('background-image', 'url(' + $img.attr('src') + ')');

				// Remove original image.
					$img.remove();

			});

	});

})(jQuery);

function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

var actualTheme = "light";

function switchTheme() {
	if (actualTheme == "dark") {
		actualTheme = "light";
		document.getElementById("iconfa").classList.remove('fa-moon-o');
		document.getElementById("iconfa").classList.add('fa-sun-o');
		var inputs = document.getElementsByTagName('iframe');
		inputs[0].src = "https://embed.twitch.tv?channel=agonatv&theme=dark";
		document.getElementById("cssID").href="assets/css/main-dark.css";
	} else {
		actualTheme = "dark";
		document.getElementById("iconfa").classList.add('fa-moon-o');
		document.getElementById("iconfa").classList.remove('fa-sun-o');
		var inputs = document.getElementsByTagName('iframe');
		inputs[0].src = "https://embed.twitch.tv?channel=agonatv&theme=light";
		document.getElementById("cssID").href="assets/css/main.css";
	}
}

function loadTwitch() {
    new Twitch.Embed("twitch-embed", {
        width: "100%",
        height: "100%",
        channel: "agonatv",
        theme: "dark"
    });
}


$(function() {
    /**
    * Smooth scrolling to page anchor on click
    **/
    $("a[href*='#']:not([href='#'])").click(function() {
        if (
            location.hostname == this.hostname
            && this.pathname.replace(/^\//,"") == location.pathname.replace(/^\//,"")
        ) {
            var anchor = $(this.hash);
            anchor = anchor.length ? anchor : $("[name=" + this.hash.slice(1) +"]");
            if ( anchor.length ) {
                $("html, body").animate( { scrollTop: anchor.offset().top }, 1500);
            }
        }
    });
});